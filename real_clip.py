import turtle
from turtle import Turtle
import numpy as np
import random
import threading
import sys
import os

_num = sys.argv[1]
num = int(_num)

length = 800
percentage = 0
path = 0
square = np.zeros(4)
drone_dis = np.zeros((num, 3))
COLOR = ["lightskyblue", "steelblue","royalblue", "darkblue", "cyan", "skyblue", "red", "black", "green", "darkred",
                        "firebrick", "slateblue"]
DIRECT = [0, 90, 180, 270]
def initialize(num):

    square[0] = length
    square[1] = length
    square[2] = 0
    square[3] = 0
    border = turtle.Turtle()
    border.penup()
    border.setpos(0, 0)
    border.pendown()
    border.goto(length, 0)
    border.goto(length, length)
    border.goto(0, length)
    border.goto(0, 0)
    border.hideturtle()

    
    client = []
    distance = np.zeros((num, num))
    for _ in range(num):
        client.append(turtle.Turtle())
    for i in range(num):
        client[i].penup()
        x = random.randint(0, 40)
        y = random.randint(0, 40)
        client[i].setpos(20*x, 20*y)
        client[i].setheading(90)
        client[i].shape("circle")
        client[i].shapesize(0.5, 0.5, 1)  
    for i in range(num):
        for j in range(num):
            distance[i][j] = client[i].distance(client[j]) / 20
    #client[0].setpos(700, 220)
    #client[1].setpos(740, 580)
    print(client[0].pos())
    print(client[1].pos())
    index = distance.argmax()
    

    drone = turtle.Turtle()
    drone.penup()
    drone.setpos(0,0)
    drone.setheading(0)
    drone.pendown()
    return client, drone
def ground_truth(client):
    tmp_dis = np.zeros((num, 1))
    ground = []
    ground_turtle = []
    minimum = 40
    for x in np.arange(0, 40, 0.5):
        for y in np.arange(0, 40, 0.5):
            tmp = (x*20, y*20)
            for i in range(num):
                tmp_dis[i] = client[i].distance(tmp) / 20
                minimum_t = np.amax(tmp_dis)
            if(minimum_t < minimum):
                #print(minimum_t)
                minimum = minimum_t
    #print("ground=", minimum)

    g_value = determine(minimum)
    print("gvalue", g_value)
    for x in np.arange(0, 40, 0.5):
        for y in np.arange(0, 40, 0.5):
            tmp = (x*20, y*20)
            for i in range(num):
                tmp_dis[i] = client[i].distance(tmp) / 20
                minimum_t = np.amax(tmp_dis)
            # if(determine(minimum_t) == g_value):
            #     ground.append(tmp)
            if(g_value == 7 or g_value == 8):
                if(determine(minimum_t) >= 7):
                    ground.append(tmp)
            if(g_value == 6):
                if(determine(minimum_t) >= 6):
                    ground.append(tmp)
            if(g_value == 5 or g_value == 4 or g_value == 3):
                if(determine(minimum_t) >= 3):
                    ground.append(tmp)

   


    for i, pos in enumerate(ground):
        ground_turtle.append(turtle.Turtle())
        ground_turtle[i].penup()
        ground_turtle[i].setpos(pos)
        ground_turtle[i].dot(5, 'red')
        ground_turtle[i].hideturtle()
    return minimum, ground_turtle, g_value
#update distance and loss

def update_distance(probe, client):
    tmp_dis = np.zeros((num, 1)) 
    for i in range(num):
        drone_dis[i][0] = probe.distance(client[i]) / 20
        tmp_dis[i][0] = probe.distance(client[i]) / 20
 
    minimum_t = np.amax(tmp_dis)
    return minimum_t
      

def change_direction(drone, deg):
    direction = drone.heading()
    new_direction = (direction + deg)
    if(new_direction > 360):
        new_direction -= 360
    if(new_direction < 0):
        new_direction += 360
    drone.setheading(new_direction)

def probe_move(probe, client, center, center_p_value):
    p_value = np.zeros(4)
    p_value[:] = center_p_value
    min_index = np.zeros(2)
    print("center p value", center_p_value)
    global path 
    for i in range(4):
        probe.penup()
        probe.setpos(center)
        probe.setheading(DIRECT[i])
        p = 0 if i%2 == 0 else 1
        while(p_value[i] == center_p_value) and (abs(probe.pos()[p]-square[i]) > 0):
            probe.forward(10)
            path += 10
            minimum_t = update_distance(probe, client)
            p_value[i] = determine(minimum_t)
        print(" i = ", i, "pos", probe.pos())
    print(p_value)
    
    min_index[0] = p_value.argsort()[3]
    min_index[1] = p_value.argsort()[2]
    
    return p_value, min_index

def determine(meters):
    meters = round(meters)
    if(meters >= 0 and meters <= 2):
        return 8
    elif(meters >= 3 and meters <= 12):
        return 7
    elif(meters >= 13 and meters <= 21):
        return 6
    elif(meters >= 22 and meters <= 25):
        return 5
    elif(meters >= 26 and meters <= 28):
        return 4
    elif(meters >= 29 and meters <= 35):
        return 3
    elif(meters >= 36 and meters <= 60):
        return 2
    else:
        return "error"


def border_update(delete, square_old, min_index, minimum, center, bad_flag, center_mini
                                    , better_number):
    print("better number", better_number)
    if(better_number == 2):
        if( 0 in min_index) and (1 in min_index):
            square[3] += abs(square_old[1]-square_old[3])/2
            square[2] += abs(square_old[0]-square_old[2])/2     
        elif( 1 in min_index) and ( 2 in min_index):
            square[0] -= abs(square_old[0]-square_old[2])/2
            square[3] += abs(square_old[1]-square_old[3])/2
        elif( 2 in min_index) and ( 3 in min_index):
            square[0] -= abs(square_old[0]-square_old[2])/2
            square[1] -= abs(square_old[1]-square_old[3])/2
        elif( 3 in min_index) and ( 0 in min_index):
            square[1] -= abs(square_old[1]-square_old[3])/2
            square[2] += abs(square_old[0]-square_old[2])/2


    if(better_number == 1):
        if(0 == min_index[0]):
            square[2] += abs(square_old[0]-square_old[2])/2
        elif(1 == min_index[0]):
            square[3] += abs(square_old[1]-square_old[3])/2
        elif(2 == min_index[0]):
            square[0] -= abs(square_old[0]-square_old[2])/2
        elif(3 == min_index[0]):
            square[1] -= abs(square_old[1]-square_old[3])/2

    print("square", square)

def result(drone, client, g_value):
    center = turtle.Turtle()
    center.penup()
    center.setpos(400, 400)
    center_p_value = 0
    centerinopti = 0
    g_TPT = 0
    center_p_value = determine(update_distance(center, client))
    if(center_p_value >= 7):
        center_TPT = 130
        if(g_value == 7 or g_value == 8):
            centerinopti = 1
    elif(center_p_value >= 6):
        center_TPT = 95
        if(g_value == 6):
            centerinopti = 1
    elif(center_p_value >= 3):
        center_TPT = 70
        if(g_value == 5 or g_value == 4 or g_value == 3):
            centerinopti = 1
    elif(center_p_value >= 2):
        center_TPT = 50
        if(g_value == 2 or g_value == 1 ):
            centerinopti = 1
    
    correct = 0
    final_TPT = 0
    final_value = determine(update_distance(drone, client))

    if(final_value >= 7):
        final_TPT = 130
        if(g_value == 7 or g_value == 8):
            correct = 1
    elif(final_value >= 6):
        final_TPT = 95
        if(g_value == 6):
            correct = 1
    elif(final_value >= 3):
        final_TPT = 70
        if(g_value == 5 or g_value == 4 or g_value == 3):
            correct = 1
    elif(final_value >= 2):
        final_TPT = 50
        if(g_value == 2 or g_value == 1 ):
            correct = 1

    if(g_value >= 8):
        g_TPT = 135
    elif(g_value >= 7):
        g_TPT = 130
    elif(g_value >= 6):
        g_TPT = 95
    elif(g_value >= 3):
        g_TPT = 70
    elif(g_value >= 1):
        g_TPT = 50






    return correct, center_TPT, final_TPT, centerinopti, g_TPT



def algo():
    global path
    turtle.tracer(0, 0)
    screen = turtle.Screen()
    screen.setup(1100, 1100)
    screen.setworldcoordinates(-200, -200, 1000, 1000)
    client, drone = initialize(num)
    g_mini, ground_turtle, g_value = ground_truth(client)
    minimum = update_distance(drone, client)
    percentage = minimum / g_mini
    radius = length


    center_turtle = turtle.Turtle()
    center_turtle.penup()
    center_turtle.setpos(length/2, length/2)

    new_center_turtle = turtle.Turtle()
    new_center_turtle.penup()
    new_center_turtle.setpos(length/2, length/2)


    probe = turtle.Turtle()
    probe.penup()
    probe.setpos(length/2, length/2)

    delete = turtle.Turtle()
    delete.penup()
    delete.pensize(2)
    delete.setpos(length/2, length/2)
    center = probe.position()
  
    epochs = 0
    square_old = np.zeros(4)
    terminate = 1
    count = 0
    counter = 0
    coefficient = 1
    while(terminate == 1):
        square_old = np.copy(square)
        print(" ")
        delete.pencolor(COLOR[epochs % 11])
        #print("color", COLOR[epochs % 11])
        delete.penup()
        probe.penup()
        probe.setpos(center)
        delete.setpos(center)
        
        
        center_mini = update_distance(center_turtle, client)
        center_p_value = determine(update_distance(center_turtle, client))
        p_value, min_index = probe_move(probe, client, center, center_p_value)
        #print("center to minimum distance", center_mini)
        bad_flag = 0
        better_number = 0
        

        for i in range(4):
            if(center_p_value > p_value[i]):
                bad_flag += 1
            elif(center_p_value < p_value[i]):
                better_number += 1

        border_update(delete, square_old, min_index, minimum, center, bad_flag, center_mini
                                    , better_number)

        
        center = ((square[0]+square[2])/2, (square[1]+square[3])/2)
        new_center_turtle.setpos(center)

        if((new_center_turtle.distance(center_turtle)/10) < 0.5):
                terminate -= 1

        if(better_number == 0):
            terminate -= 1
            # if( abs(square[0]-square[2]) < 50) or (abs(square[1]-square[3]) < 50):
            #     terminate -= 1     
        center_turtle.setpos(center)

        
        path += center_turtle.distance(drone) / 10
        drone.penup()
        drone.setpos(center)
        drone.pendown()
        drone.dot(8, "green")
        drone.hideturtle()
        

        minimum = update_distance(drone, client)
        epochs += 1
    

    #print("distance to m", update_distance(drone, client))
    
    
    correct, center_TPT, final_TPT, centerinopti, g_TPT = result(drone, client, g_value)
    
    # print("correct", correct)
    # print("center throughput", center_TPT)
    # print("final throughput", final_TPT)
    # print("improve", round(final_TPT / center_TPT, 2))
    # print("path", path/10)
    output = ""
    output += str(path/10)
    output += ',' + str(g_TPT)
    #output += ',' + str(centerinopti)
    output += ',' + str(center_TPT) 
    output += ',' + str(final_TPT)
    #output += ',' + str(round(final_TPT / center_TPT, 2)) 
    output += ',' + str(correct) + '\n'
    print(output)
    #print("path=", round((path/10), 2))
    filename = _num + '.csv'
    fp = open(filename, 'a')
    fp.write(output)

    turtle.update()
    #screen.exitonclick()
    #print(percentage)
    #return percentage

if __name__ == '__main__':
    algo()
    