reset

set output "./real_dis.eps"
set terminal postscript eps enhanced color  "Times-Roman" 28
set size 1,0.8

#set style data linespoints
unset label

set xrange [2:20]
set xlabel "Number of users" offset 0,0.5
set yrange [60:120]
set ylabel "Meters" offset 2
set ytics 0, 10


set xtics 1  offset 0, 0.1

set border lw 2
set key samplen 1.5
#set key width -0.5
set key left horiz top font ",28"

plot "path.txt" u ($0+2):1 with line title "UAV moving distance" lw 4 lc rgb "red"
     