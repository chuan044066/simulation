reset

set output "./real_tpt.eps"
set terminal postscript eps enhanced color  "Times-Roman" 28
set size 1,0.8

#set style data linespoints
unset label

set xrange [2:20]
set xlabel "Number of users" offset 0,0.5
set yrange [60:120]
set ylabel "Minimum throughput (Mbps)" offset 2
set ytics 0, 10


set xtics 1  offset 0, 0.1

set border lw 2
set key samplen 1.5
#set key width -0.5
#set key left horiz top font ",28"

plot "center.txt" u ($0+2):1 with line title "Center (default)" lw 4 lc rgb	"blue", \
     "output.txt" u ($0+2):1 with line title "DPF" lw 4 lc rgb "red", \
     "ground.txt" u ($0+2):1 with line title "Exhaustive" lw 4 lc rgb "dark-green"
