import turtle
from turtle import Turtle
import numpy as np
import random
import threading
import sys
import os

_num = sys.argv[1]
num = int(_num)
length = 800
percentage = 0
path = 0
square = np.zeros(4)
drone_dis = np.zeros((num, 3))
COLOR = ["lightskyblue", "steelblue","royalblue", "darkblue", "cyan", "skyblue", "red", "black", "green", "darkred",
                        "firebrick", "slateblue"]
DIRECT = [0, 90, 180, 270]
def initialize(num):

    #square = [(length, length), (-length, length), (-length, -length), (length, -length)]
    square[0] = length
    square[1] = length
    square[2] = 0
    square[3] = 0
    border = turtle.Turtle()
    border.penup()
    border.setpos(0, 0)
    border.pendown()
    border.goto(length, 0)
    border.goto(length, length)
    border.goto(0, length)
    border.goto(0, 0)
    border.hideturtle()

    # border1 = turtle.Turtle()
    # border1.penup()
    # border1.setpos(0, -220)
    # border1.pendown()
    # border1.goto(0, 220)
    # border1.hideturtle()


    # border2 = turtle.Turtle()
    # border2.penup()
    # border2.setpos(-220, 0)
    # border2.pendown()
    # border2.goto(220, 0)
    # border2.hideturtle()


    client = []
    distance = np.zeros((num, num))
    for _ in range(num):
        client.append(turtle.Turtle())
    for i in range(num):
        client[i].penup()
        x = random.randint(0, 40)
        y = random.randint(0, 40)
        client[i].setpos(20*x, 20*y)
        client[i].setheading(90)
        client[i].shape("circle")
        client[i].shapesize(0.5, 0.5, 1)  
    for i in range(num):
        for j in range(num):
            distance[i][j] = client[i].distance(client[j]) / 20
    #client[0].setpos(760, 80)
    #client[1].setpos(520, 620)
    print(client[0].pos())
    print(client[1].pos())
    index = distance.argmax()
    

    drone = turtle.Turtle()
    drone.penup()
    drone.setpos(400,400)
    drone.setheading(0)
    drone.pendown()
    return client, drone
def ground_truth(client):
    tmp_dis = np.zeros((num, 1))
    ground = []
    ground_turtle = []
    minimum = 40
    for x in np.arange(0, 40, 0.5):
        for y in np.arange(0, 40, 0.5):
            tmp = (x*20, y*20)
            for i in range(num):
                tmp_dis[i] = client[i].distance(tmp) / 20
                minimum_t = np.amax(tmp_dis)
            if(minimum_t < minimum):
                #print(minimum_t)
                minimum = minimum_t
    #print("ground=", minimum)


    for x in np.arange(0, 40, 0.5):
        for y in np.arange(0, 40, 0.5):
            tmp = (x*20, y*20)
            for i in range(num):
                tmp_dis[i] = client[i].distance(tmp) / 20
                minimum_t = np.amax(tmp_dis)
            if(abs(minimum_t - minimum) < 0.05):
                #print(minimum_t)
                ground.append(tmp)

   


    for i, pos in enumerate(ground):
        ground_turtle.append(turtle.Turtle())
        ground_turtle[i].penup()
        ground_turtle[i].setpos(pos)
        ground_turtle[i].dot(5, 'red')
        ground_turtle[i].hideturtle()
    return minimum, ground_turtle
#update distance and loss
def update_distance(probe, client):
    tmp_dis = np.zeros((num, 1)) 
    for i in range(num):
        drone_dis[i][0] = probe.distance(client[i]) / 20
        tmp_dis[i][0] = probe.distance(client[i]) / 20
 
    minimum_t = np.amax(tmp_dis)
    #print(drone_dis)
    return minimum_t
      

def change_direction(drone, deg):
    direction = drone.heading()
    new_direction = (direction + deg)
    if(new_direction > 360):
        new_direction -= 360
    if(new_direction < 0):
        new_direction += 360
    drone.setheading(new_direction)

def probe_move(probe, client, center, coefficient):
    min_in_one_round = np.zeros(4)
    min_index = np.zeros(2)
    global path 
    for i in range(4):
        if(i%2 == 0):
            radius = abs(square[0]-square[2])/4
            a = radius
        elif(i%2 == 1):
            radius = abs(square[1]-square[3])/4
            b = radius
        radius *= coefficient  #!!!
        probe.setheading(DIRECT[i])
        probe.forward(radius)
        minimum_t = update_distance(probe, client)
        min_in_one_round[i] = minimum_t
        probe.penup()
        probe.setpos(center)
        change_direction(probe, DIRECT[i])
    if(a < b):
        a, b = b, a
    path += a + 2 * (a**2 + b**2) ** (0.5) + 2 * b
    
    
    min_index[0] = min_in_one_round.argsort()[0]
    min_index[1] = min_in_one_round.argsort()[1]
    
    return min_in_one_round, min_index

def border_update(delete, min_in_one_round, min_index, square_old, minimum, center, flag, center_mini, better_number):
    
    print("min in one round", min_in_one_round)
    print("min index", min_index)
    print("min better_number", better_number)
    if(better_number == 2):
        if( 0 in min_index) and (1 in min_index):
            square[3] += abs(square_old[1]-square_old[3])/2
            square[2] += abs(square_old[0]-square_old[2])/2     
        elif( 1 in min_index) and ( 2 in min_index):
            square[0] -= abs(square_old[0]-square_old[2])/2
            square[3] += abs(square_old[1]-square_old[3])/2
        elif( 2 in min_index) and ( 3 in min_index):
            square[0] -= abs(square_old[0]-square_old[2])/2
            square[1] -= abs(square_old[1]-square_old[3])/2
        elif( 3 in min_index) and ( 0 in min_index):
            square[1] -= abs(square_old[1]-square_old[3])/2
            square[2] += abs(square_old[0]-square_old[2])/2


    if(better_number == 1):
        if(0 == min_index[0]):
            square[2] += abs(square_old[0]-square_old[2])/2
        elif(1 == min_index[0]):
            square[3] += abs(square_old[1]-square_old[3])/2
        elif(2 == min_index[0]):
            square[0] -= abs(square_old[0]-square_old[2])/2
        elif(3 == min_index[0]):
            square[1] -= abs(square_old[1]-square_old[3])/2

    if(better_number == 0):
        square[2] += abs(square_old[0]-square_old[2])/4
        square[3] += abs(square_old[1]-square_old[3])/4
        square[0] -= abs(square_old[0]-square_old[2])/4
        square[1] -= abs(square_old[1]-square_old[3])/4
    print("square", square)
    # for index in min_index:
    #     i = int(index)
    #     if(index%2 == 1):
    #         radius = abs(square_old[0]-square_old[2])/4
    #     elif(index%2 == 0):
    #         radius = abs(square_old[1]-square_old[3])/4
    #     # if(min_in_one_round[i] > minimum):
    #     delete.setheading(DIRECT[i])
    #     delete.forward(radius)
    #     if(i%2 == 0):
    #         square[i] = delete.position()[0]
    #     elif(i%2 == 1):
    #         square[i] = delete.position()[1]
    #     delete.pendown()
    #     change_direction(delete, 90)
    #     delete.forward(radius*2)
    #     delete.backward(radius*4)
    #     delete.forward(radius*2)
    #     delete.hideturtle()
    #     delete.penup()
    #     delete.setpos(center)

def algo():
    
    #screen = turtle.Screen()
    #screen.setup(1100, 1100)
    turtle.tracer(0, 0)
    #screen.setworldcoordinates(-200, -200, 1000, 1000)
    client, drone = initialize(num)
    g_mini, ground_turtle = ground_truth(client)
    minimum = update_distance(drone, client)
    g_minimum = update_distance(ground_turtle[0], client)
    #print("start point minumum = ", minimum)
    percentage = minimum / g_mini
    radius = length

    output = ""
    output = str(round(g_minimum,2))
    output += ',' + str(round(minimum, 2))


    center_turtle = turtle.Turtle()
    center_turtle.penup()
    center_turtle.setpos(length/2, length/2)

    new_center_turtle = turtle.Turtle()
    new_center_turtle.penup()
    new_center_turtle.setpos(length/2, length/2)


    probe = turtle.Turtle()
    probe.penup()
    probe.setpos(length/2, length/2)

    delete = turtle.Turtle()
    delete.penup()
    delete.pensize(2)
    delete.setpos(length/2, length/2)
    center = probe.position()
    #print(type(center))
    #print(center)
    #print(center[0])

    epochs = 0
    square_old = np.zeros(4)
    terminate = 1
    count = 0
    counter = 0
    coefficient = 1
    while(terminate == 1):
        square_old = np.copy(square)
        print(" ")
        delete.pencolor(COLOR[epochs % 11])
        #print("color", COLOR[epochs % 11])
        delete.penup()
        probe.penup()
        probe.setpos(center)
        delete.setpos(center)
        
        
        center_mini = update_distance(new_center_turtle, client)
        min_in_one_round, min_index = probe_move(probe, client, center, coefficient)
        print("center to minimum distance", center_mini)
        
        #print("delete bound index", min_index)
        # test = 0
        # for i in range(4):
        #     if(center_mini > min_in_one_round[i]):
        #         test += 1
        # print("how many", test)

        bad_flag = 0
        better_number = 0
        for i in range(4):
            if(center_mini > min_in_one_round[i]):
                better_number += 1
        
        border_update(delete, min_in_one_round, min_index, square_old, minimum, center, bad_flag, center_mini
                                    , better_number)

        
        
        center = ((square[0]+square[2])/2, (square[1]+square[3])/2)
        print("center", center)
        new_center_turtle.setpos(center)
        #print("new center", new_center_turtle.pos())
        if((new_center_turtle.distance(center_turtle)/10) < 0.5):
            terminate -= 1
        if(better_number == 0):
            terminate += 1
            if( abs(square[0]-square[2]) < 50) or (abs(square[1]-square[3]) < 50):
                terminate -= 1
        center_turtle.setpos(center)


        #path += center_turtle.distance(drone) / 20
        drone.penup()
        drone.setpos(center)
        drone.pendown()
        drone.dot(8, "green")
        drone.hideturtle()
        

        minimum = update_distance(drone, client)
        epochs += 1
    d_to_g = 100
    # for i in range(len(ground_turtle)):
    #     if((drone.distance(ground_turtle[i])/20) < d_to_g):
    #         d_to_g = (drone.distance(ground_turtle[i])/20)
    #print("distance to g", d_to_g)

    

    #print("distance to m", update_distance(drone, client))
    #print("path=", round((path/10), 2))

    center_turtle.setpos(center)
    final_dis = update_distance(center_turtle, client)
    output += ',' + str(round(final_dis, 2))
    output += ',' + str(round((path/10), 2)) + '\n'
    print(output)

    filename = _num + '.csv'
    fp = open(filename, 'a')
    fp.write(output)
    turtle.update()

    #screen.exitonclick()
    #print(percentage)
    #return percentage

if __name__ == '__main__':
    algo()
    