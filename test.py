import sys
import os
import random

 
forks = 50
def forkk(command):
    for i in range(forks):
        try:
            pid = os.fork()
        except OSError:
            sys.stderr.write("Could not create a child process\n")
            continue
        
        if pid == 0:
            print("In the child process {} that has the PID {}".format(i+1, os.getpid()))
            os.system(command)
            exit()
        else:
            print("In the parent process after forking the child {}".format(pid))
    
    print("In the parent process after forking {} children".format(forks))
    
    for i in range(forks):
        finished = os.waitpid(0, 0)
        print(finished)

if __name__ == '__main__':
    for j in range(10,21):
        command = "python3 ideal_clip.py "
        command += str(j)
        for i in range(20):
            forkk(command)
