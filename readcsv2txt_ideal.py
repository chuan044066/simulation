import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import math
import itertools
ground = []
center = []
output = []
uav_path = []
index = []


os.chdir("./ideal")
colnames = ['dis_g', 'dis_c', 'dis_o', 'path']
#colnames_real = ['path', 'g_TPT', 'c_TPT', 'o_TPT','acc'] 
def FSPL(meters):
    dB = 20*math.log(meters, 10) + 20*math.log(5480, 10) - 27.55
    return dB


fg = open("/home/chuan/simulation/ideal_plot/ground.txt", "w")
fc = open("/home/chuan/simulation/ideal_plot/center.txt", "w")
fo = open("/home/chuan/simulation/ideal_plot/output.txt", "w")
fp = open("/home/chuan/simulation/ideal_plot/path.txt", "w")
for i in range(2, 21):
    print(i)
    _file = str(i) + '.csv'  
    df_data = pd.read_csv(_file, names=colnames)
    print(df_data)

    ground.append(FSPL(df_data["dis_g"].mean()))

    center.append(FSPL(df_data["dis_c"].mean()))
    

    output.append(FSPL(df_data["dis_o"].mean()))
    

    uav_path.append(df_data["path"].mean())
    index.append(i)
print(ground)
print(center)
print(output)

for v in ground:
    value = str(v) + '\n'
    fg.write(value)

for v in center:
    value = str(v) + '\n'
    fc.write(value)

for v in output:
    value = str(v) + '\n'
    fo.write(value)

for v in uav_path:
    value = str(v) + '\n'
    fp.write(value)


# fig, ax = plt.subplots(1)
# plt.xticks(np.arange(2, 21, 1))
# plt.xlabel("Number of users", fontsize=13)
# h = plt.ylabel("Minimum throughput (Mbps)", fontsize=13)
# #h.set_rotation(0)
# MARKER = itertools.cycle(('o', 'x', '^'))
# plt.plot(index, ground, label='Exhaustive', marker=next(MARKER))
# plt.plot(index, output, label='DPF', marker=next(MARKER))
# plt.plot(index, center, label='Center (default)', marker=next(MARKER))
# #plt.plot(index, uav_path)
# plt.legend(['UAV moving distance'], fontsize=14)
# handles,labels = ax.get_legend_handles_labels()
# handles = [handles[0], handles[1], handles[2]]
# labels = [labels[0], labels[1], labels[2]]

# ax.legend(handles,labels, fontsize=12)
# #plt.legend(fontsize=12)
# plt.figure(dpi=300)
# plt.show()
